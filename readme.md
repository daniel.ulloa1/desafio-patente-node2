## How to try
```sh

  npm run docker:build
  npm run docker:run

```
## Example getId
```sh
Post  http://localhost:8000/api/patente/getId
body = {
  "patente":"ZZZZ999"
}
```
> output: 456976000
**
## Example getPatente
```sh
Post  http://localhost:8000/api/patente/getPatente
body = {
  "id": 456976000
}
```
> output: ZZZZ999
**


made with love :heart: