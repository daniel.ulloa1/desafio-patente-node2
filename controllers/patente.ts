import { Request, Response } from "express";
const { generatorAlphaUpper } = require('unique-sequence');

export const getId = async( req: Request , res: Response ) => {
    if (!req.body && !req.body.patente){
      return res.status(400).json('Ingresa un campo patente valido');
    }
    try {
      const { patente } = req.body;
      const id = patenteToId(patente);
      res.json( id );
    } catch (error) {
      res.status(500).json(error);
    }
}

const patenteToId = (patente: string) => {  
  let patenteArr = patente.split('');
  let numeros = patenteArr.slice(patenteArr.length - 3);
  let letters = patenteArr.slice(0, patenteArr.length - 3);
  if (letters.length !== 4 || numeros.length !== 3 || !letters.every(element => element.match(/[A-Z]/))) {
    return "Error, ingrese una patente valida";
  }
  if (!numeros.every(element => element.match(/[0-9]/))) {
    return "Error, ingrese una patente valida";
  }

  let letterInNumber = convertLetterToNumber(letters.join(""));
  console.log("letterInNumber-------> ", letterInNumber);
  let id = letterInNumber*1000 + parseInt(numeros.join(""))+1;
  return id;
}
function convertLetterToNumber(letter: string) {
    var out = 0, len = letter.length;
      for (var i = 0; i < len; i++) {
        out += (letter.charCodeAt(i) - 65) * Math.pow(26, len - i - 1);
      }
      return out;
  }




export const getPatente = async( req: Request , res: Response ) => {
    if (!req.body && !req.body.patente){
      return res.status(400).json('Ingresa un campo patente valido');
    }
    try { 
      const { id } = req.body;
      const patente = idToPatente(id);
      res.json( patente );
    } catch (error) {
      res.status(500).json(error);
    }
}


const idToPatente = (id: any) => {

    if ( !id.toString().match(/[0-9]/)) {
      return "Error, ingrese un id valido";
    }
    let patente = "";
    id= parseInt(id) - 1;
    let idSplit = id.toString().split("");
    //si id es menor a 1000 patente = AAAA000 + id
    const gen  = generatorAlphaUpper()
    let idLetter = gen.next().value
    let idNumber = "000"
    //separar arreglo en 3 ultimos digitos
    let idSplitNumber = idSplit.slice(idSplit.length - 3);
    let idSplitLetter;
    let letterInNumber = 0;
    //separar arreglo hasta los 3 ultimos digitos
    if(idSplit.length > 3){
        idSplitLetter = idSplit.slice(0, idSplit.length - 3);
        letterInNumber = idSplitLetter.join("");
        console.log("letterInNumber----------> ", letterInNumber);
        for(let index = 0; index < letterInNumber; index++){
            idLetter = gen.next().value;
        }
    }
    while(idLetter.length < 4){
        idLetter = "A" + idLetter;
    }
    if(idLetter.length > 4){
       return 'Error, ingrese un id valido';
    }
    
    for(let i = 0; i < idSplitNumber.length; i++){
            idNumber += idSplitNumber[i] ;
    }
    let idNumberSplit = parseInt(idNumber).toString()
    let idNumberArray = idNumberSplit.split("");
    while(idNumberArray.length < 3){
        idNumberArray.unshift("0");
    }
    let idNumberString = idNumberArray.join("");
    patente = idLetter.toUpperCase() + idNumberString;
    console.log("patente----------> ", patente);

    return patente;
}

