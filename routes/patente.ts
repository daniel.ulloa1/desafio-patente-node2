import { Router } from 'express';
import { getPatente, getId } from '../controllers/patente';

const router = Router();


router.post('/getId', getId );
router.post('/getPatente', getPatente );



export default router;